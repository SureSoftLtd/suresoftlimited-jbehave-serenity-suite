# SureSoft Limited Test Suite - Java, JBehave, Serenity, Selenium

## This test-suite utilises the following

 - JBehave (The Java-based BDD framework focused on stories)
 - Serenity (Formerly Thucydides. An open-source acceptance-test library. Used either with a BDD tool such as JBehave 
 or Cucumber, or with plain Java using JUnit)
 - Selenium (An umbrella project for a range of tools and libraries enabling the automation of web browsers)
 - Maven (A software project management tool, based on Project Object Model (POM), it can manage the build, reporting,
 and documentation of a project)

## To do the following

 - Demonstrate the use of BDD tools to facilitate the creation of a collaboration framework.

### Prerequisites

 - JDK must be installed
 - Maven must be installed and on the path
 - ChromeDriver must be installed and on the path

### To run

 - mvn clean verify - will run tests in Chrome after deleting the target directory. A post-integration-test phase aggregates 
 the reports for you.
 
### Docs

[Selenium](http://www.seleniumhq.org/docs/index.jsp)  
[Serenity](http://thucydides.info/docs/serenity-staging/)  
[JBehave](http://jbehave.org/) 

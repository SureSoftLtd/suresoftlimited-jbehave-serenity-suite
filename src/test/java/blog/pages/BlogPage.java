package blog.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;

@DefaultUrl("https://suresoftlimited.com/blog")
public class BlogPage extends PageObject {

    public BlogPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id="blog_subscription-3")
    WebElementFacade blogSubscription;

    @FindBy(id="subscribe-field")
    WebElementFacade emailField;

    @FindBy(xpath="//*[@id=\"subscribe-blog\"]/p[3]/button")
    WebElementFacade submitButton;

    @FindBy(xpath="//*[@id=\"blog_subscription-3\"]/div")
    WebElementFacade subscriptionThankyouMessage;

    @FindBy(xpath="//*[@id=\"actionbar\"]/ul/li[1]/div/div[2]")
    WebElementFacade subscriptionThankyouPopup;

    public boolean blogSubscriptionAvailable() {
        return blogSubscription.isDisplayed();
    }

    public void enterEmail(String email) {
        emailField.sendKeys(email);
        submitButton.click();
    }

    public boolean subscriptionThankyouPopupShown() {
        return subscriptionThankyouPopup.isDisplayed();
    }

    public boolean subscriptionThankyouMessageShown() {
        return subscriptionThankyouMessage.isDisplayed();
    }

    public boolean subscriptionEmailSent() {
        // todo: not implemented yet. Integration needed with email sender for this to pass.
        return false;
    }
}


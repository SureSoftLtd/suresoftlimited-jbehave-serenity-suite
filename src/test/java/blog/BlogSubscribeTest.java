package blog;

import blog.BlogSubscribeStepLib;

import net.thucydides.core.annotations.Steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BlogSubscribeTest {

    @Steps
    BlogSubscribeStepLib blogSubscribeStepLib;

    @Given("Joe visits our website blog page")
    public void joe_visits_our_website_blog_page() {
        blogSubscribeStepLib.openBlogPage();
    }

    @Given("they see the blog subscription widget asking for an email address")
    public void they_see_the_blog_subscription_widget() {
        blogSubscribeStepLib.blogSubscriptionAvailable();
    }

    @When("they enter suresoft.qa@gmail.com into the subscription widget")
    public void they_enter_email_into_subscription_widget() { // @Named("email")String email)
        blogSubscribeStepLib.enterEmail("suresoft.qa@gmail.com");
    }

    @Then("Joe sees a message in the widget thanking him")
    public void joe_sees_a_thankyou_message() {
        assertThat(blogSubscribeStepLib.subscriptionThankyouMessageShown(), equalTo(true));
    }

    @Then("a popup saying thanks")
    public void joe_sees_a_thankyou_popup() {
        assertThat(blogSubscribeStepLib.subscriptionThankyouPopupShown(), equalTo(true));
    }

    @Then("an email is sent to him about his subscription")
    public void an_email_is_sent_about_their_subscription() {
        assertThat(blogSubscribeStepLib.subscriptionEmailSent(), equalTo(true));
    }

}
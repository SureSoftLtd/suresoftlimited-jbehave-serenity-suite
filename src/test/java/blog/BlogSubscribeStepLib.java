package blog;

import blog.pages.BlogPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

public class BlogSubscribeStepLib extends ScenarioSteps {

    BlogPage blogPage;

    public BlogSubscribeStepLib(Pages pages) {
        super(pages);
    }

    @Step
    public void openBlogPage() {
        blogPage.open();
    }

    @Step
    public boolean blogSubscriptionAvailable() {
        return blogPage.blogSubscriptionAvailable();
    }

    @Step
    public void enterEmail(String email) {
        blogPage.enterEmail(email);
    }

    @Step
    public boolean subscriptionThankyouMessageShown() {
        return blogPage.subscriptionThankyouMessageShown();
    } 

    @Step
    public boolean subscriptionThankyouPopupShown() {
        return blogPage.subscriptionThankyouPopupShown();
    } 

    @Step
    public boolean subscriptionEmailSent() {
        return blogPage.subscriptionEmailSent();
    }     
}

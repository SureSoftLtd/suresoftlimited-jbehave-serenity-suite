A blog reader can subscribe to the blog

Narrative:
In order to know when new posts are published
As a reader of the blog
I want to be able to subscribe to it

Scenario: Blog reader attempts to subscribe using a valid email address

Given Joe visits our website blog page
And they see the blog subscription widget asking for an email address
When they enter suresoft.qa@gmail.com into the subscription widget
Then Joe sees a message in the widget thanking him
And a popup saying thanks
And an email is sent to him about his subscription